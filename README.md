**DEPRECATED**: As of version 0.4, the `rsa` crate supports exporting keys [natively](https://docs.rs/rsa/0.4.0/rsa/trait.PrivateKeyPemEncoding.html). Please use this over `rsa_export`, if you can!

`rsa_export` allows you to export your RSA key, generated via the [rsa crate](https://crates.io/crates/rsa), with PKCS#1 or PKCS#8 encoding  

Reference: https://tools.ietf.org/html/rfc3447#appendix-A.1

**Note**: Multi-prime keys are not supported

The keys can also be exported into the PEM format by enabling the feature `pem`

Example:  

```rust
use rsa::RSAPrivateKey;
use rand::rngs::OsRng;
use rsa_export::Encode;

let mut rng = OsRng;
let private_key = RSAPrivateKey::new(&mut rng, 2048).unwrap();
let public_key = private_key.to_public_key();

let pkcs1_encoded_private = private_key.as_pkcs1().unwrap();
let pkcs1_encoded_public = public_key.as_pkcs1().unwrap();

let pkcs8_encoded_private = private_key.as_pkcs8().unwrap();
let pkcs8_encoded_public = public_key.as_pkcs8().unwrap();
```

Encode PKCS#1 or PKCS#8 encoded keys into the PEM format (with the `pem` feature enabled)

```rust
use rsa::RSAPrivateKey;
use rand::rngs::OsRng;
use rsa_export::PemEncode;

let mut rng = OsRng;
let private_key = RSAPrivateKey::new(&mut rng, 2048).unwrap();
let public_key = private_key.to_public_key();

let pkcs1_encoded_private_pem = private_key.as_pkcs1_pem().unwrap();
let pkcs1_encoded_public_pem = public_key.as_pkcs1_pem().unwrap();

let pkcs8_encoded_private_pem = private_key.as_pkcs8_pem().unwrap();
let pkcs8_encoded_public_pem = public_key.as_pkcs8_pem().unwrap();
```