use {
    rand::rngs::OsRng,
    rsa::RSAPrivateKey,
    rsa_export::{LineEnding, PemEncode},
};

fn main() {
    let mut rng = OsRng;
    let private_key = RSAPrivateKey::new(&mut rng, 2048).unwrap();
    let public_key = private_key.to_public_key();

    // Default unix-style line ending
    let pem_pkcs1_encoded_private = private_key.as_pkcs1_pem().unwrap();
    let pem_pkcs1_encoded_public = public_key.as_pkcs1_pem().unwrap();

    println!("Unix-style line endings:");
    println!("PEM PKCS#1 encoded private key:");
    println!("{:?}", pem_pkcs1_encoded_private);
    println!("\nPEM PKCS#1 encoded public key:");
    println!("{:?}", pem_pkcs1_encoded_public);

    // Windows-style line ending
    let pem_pkcs1_encoded_private = private_key
        .as_pkcs1_pem_custom_ending(LineEnding::CRLF)
        .unwrap();
    let pem_pkcs1_encoded_public = public_key
        .as_pkcs1_pem_custom_ending(LineEnding::CRLF)
        .unwrap();

    println!("Windows-style line endings:");
    println!("PEM PKCS#1 encoded private key:");
    println!("{:?}", pem_pkcs1_encoded_private);
    println!("\nPEM PKCS#1 encoded public key:");
    println!("{:?}", pem_pkcs1_encoded_public);
}
