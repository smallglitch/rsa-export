use {rand::rngs::OsRng, rsa::RSAPrivateKey, rsa_export::PemEncode};

fn main() {
    let mut rng = OsRng;
    let private_key = RSAPrivateKey::new(&mut rng, 2048).unwrap();
    let public_key = private_key.to_public_key();

    let pem_pkcs1_encoded_private = private_key.as_pkcs1_pem().unwrap();
    let pem_pkcs1_encoded_public = public_key.as_pkcs1_pem().unwrap();

    println!("PEM PKCS#1 encoded private key:");
    println!("{}", pem_pkcs1_encoded_private);
    println!("\nPEM PKCS#1 encoded public key:");
    println!("{}", pem_pkcs1_encoded_public);
}
