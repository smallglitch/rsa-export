use {
    simple_asn1::ASN1EncodeErr,
    std::{error::Error as StdError, fmt},
};

/// Combined error type
#[derive(Debug)]
pub enum Error {
    /// ASN.1 encode error variant
    Asn1Encode(ASN1EncodeErr),
    /// Mod inverse fail variant
    ModInverse,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Asn1Encode(err) => write!(f, "ASN.1 encode error: {}", err),
            Error::ModInverse => write!(f, "Mod inverse failed"),
        }
    }
}

impl StdError for Error {}

impl From<ASN1EncodeErr> for Error {
    fn from(err: ASN1EncodeErr) -> Self {
        Error::Asn1Encode(err)
    }
}
