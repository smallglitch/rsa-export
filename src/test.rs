use crate::Encode;

#[test]
fn export_private_pkcs1() {
    let mut rng = rand::rngs::OsRng;

    let key = rsa::RSAPrivateKey::new(&mut rng, 128).unwrap();
    let encoded_key = key.as_pkcs1().unwrap();

    let parsed_key = rsa::RSAPrivateKey::from_pkcs1(encoded_key.as_slice()).unwrap();

    assert_eq!(key, parsed_key);
}

#[test]
fn export_private_pkcs8() {
    let mut rng = rand::rngs::OsRng;

    let key = rsa::RSAPrivateKey::new(&mut rng, 128).unwrap();
    let encoded_key = key.as_pkcs8().unwrap();

    let parsed_key = rsa::RSAPrivateKey::from_pkcs8(encoded_key.as_slice()).unwrap();

    assert_eq!(key, parsed_key);
}

#[test]
fn export_public_pkcs1() {
    let mut rng = rand::rngs::OsRng;

    let key = rsa::RSAPrivateKey::new(&mut rng, 128).unwrap();
    let key = key.to_public_key();
    let encoded_key = key.as_pkcs1().unwrap();

    let parsed_key = rsa::RSAPublicKey::from_pkcs1(encoded_key.as_slice()).unwrap();

    assert_eq!(key, parsed_key);
}

#[test]
fn export_public_pkcs8() {
    let mut rng = rand::rngs::OsRng;

    let key = rsa::RSAPrivateKey::new(&mut rng, 128).unwrap();
    let key = key.to_public_key();
    let encoded_key = key.as_pkcs8().unwrap();

    let parsed_key = rsa::RSAPublicKey::from_pkcs8(encoded_key.as_slice()).unwrap();

    assert_eq!(key, parsed_key);
}
